package com.company;

import java.util.ArrayList;

public class Dado {
    ArrayList<Integer> valores = new ArrayList<>();

    public ArrayList<Integer> getValores() {
        return this.valores;
    }

    public void criarDado(int lados){
        for (int i = 1; i < (lados+1); i++){
            valores.add(i);
        }
//        System.out.println("Seu dado tem os numeros abaixo: ");
//        System.out.println(valores);
    }
}
